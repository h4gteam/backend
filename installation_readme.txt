Instructions to install rbenv:

https://github.com/rbenv/rbenv
Then install ruby-build

commands:

rbenv install 2.2.0
rbenv global 2.2.0
gem install bundler
