class AddPupils < ActiveRecord::Migration
  def change
  	create_table(:pupils) do |t|
  		t.string :name
  		t.string :description
  		t.string :photo
  		t.integer :teacher_id
  		t.string  :assigment_id, array: true
  		t.integer :badges_list, array: true
  	end 

  end
end
