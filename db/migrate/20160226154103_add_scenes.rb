class AddScenes < ActiveRecord::Migration
  def change
  	  	create_table(:scenes) do |t|
  		t.string :name
  		t.string :description
  		t.integer :dificulty
  		t.integer :badge_id
  		t.integer :types
  	end 
  end
end
