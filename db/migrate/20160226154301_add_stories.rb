class AddStories < ActiveRecord::Migration
  def change
  	  	create_table(:stories) do |t|
  		t.integer :owner_id
  		t.string :name
  		t.string :description
  		t.integer :dificulty
  		t.integer :scenes, array: true
  	end 
  end
end
