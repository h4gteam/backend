class TeachersController < ApplicationController

	def index
		render "teachers/index"
	end

	def new_pupil
		render "teachers/new_pupil"
	end

	def pupils
		render "teachers/pupils"
	end

	def show_pupil
		render "teachers/show_pupil"
	end

end