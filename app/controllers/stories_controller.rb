 class StoriesController < ApplicationController

	 def index
	  	render "stories/index"
	 end

	def new
		render "stories/new"
	end

	def show
		render "stories/show_story"
	end
end